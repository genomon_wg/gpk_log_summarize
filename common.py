#!/usr/bin/env python

import math
import os
import re
import sys

re_time = re.compile('@time\[(.*)\] time\(real,user,sys\) = (\S+)\s(\S+)\s(\S+)')


command_list = [
    'bamtofastq',
    'split ',
    'count_lines ',
    'bwa_mem',
    'split_into_chrs',
    'scatter_sam',
    'cat',
    'bamsort',
    'bammarkduplicates',
    'remove_chr_dir',
    'samtools cat',
    'bai_merge',
    'samtools index',
    'md5sum',
    'EBFilter',
    'cp',
]


def normalize_command_name(str):
    for command in command_list:
        if str.startswith(command):
            return command.rstrip()
    return str


def get_time(file_name):
    time_list = []
    f = open(file_name, 'r')
    for line in f:
        m = re_time.match(line)
        if m:
            item = {'command': m.group(1),
                    'real': float(m.group(2)),
                    'user': float(m.group(3)),
                    'sys':  float(m.group(4))}
            item['command'] = normalize_command_name(item['command'])
            time_list.append(item)
    return time_list


def get_accumulate_time(file_name):
    time_list = {}
    f = open(file_name, 'r')
    for line in f:
        m = re_time.match(line)
        if m:
            command = normalize_command_name(m.group(1))
            real = float(m.group(2))
            user = float(m.group(3))
            sys  = float(m.group(4))
            if not command in time_list:
                time_list[command] = {'real': 0, 'user': 0, 'sys': 0}
            time_list[command]['real'] += real
            time_list[command]['user'] += user
            time_list[command]['sys' ] += sys
    return time_list


def summarize_total_time(index, job_list):
    num_job = len(index)
    tmin = float('inf')
    tmax = 0
    tsum = 0
    for job_id in index:
        job = job_list[job_id]
        assert job_id == job['id']
        time = job['time']
        if time > tmax:
            tmax = time
            tmax_worker = job['worker']
            tmax_bulkjob_id = job['bulkjob_id']
            tmax_id = job_id
        if time < tmin:
            tmin = time
            tmin_worker = job['worker']
            tmin_bulkjob_id = job['bulkjob_id']
        tsum += time
    summary = {'tmax': tmax, 'tmin': tmin, 'tave': tsum/num_job,
               'tmax_id': tmax_id,
               'tmax_worker': tmax_worker, 'tmax_bulkjob_id': tmax_bulkjob_id,
               'tmin_worker': tmin_worker, 'tmin_bulkjob_id': tmin_bulkjob_id}
    return summary


def print_total_time_summary(summary):
    print('min: %.3f @(worker:%d, bulkjob_id:%d)'
          % (summary['tmin'], summary['tmin_worker'], summary['tmin_bulkjob_id']))
    print('max: %.3f @(worker:%d, bulkjob_id:%d)'
          % (summary['tmax'], summary['tmax_worker'], summary['tmax_bulkjob_id']))
    print('ave: %.3f' % summary['tave'])


def summarize_command(index, job_list, vge_output_dir):
    num_job = len(index)
    summaries = {}
    for job_id in index:
        job = job_list[job_id]
        assert job_id == job['id']
        file_name = vge_output_dir + job['file'] + '.e' + str(job_id)
        time_list = get_time(file_name)
        for item in time_list:
            command = item['command']
            if not command in summaries:
                summaries[command] = {'tmax': 0, 'tmin': float('inf'), 'tave': 0,
                                      'tsd': 0,
                                      'tmax_user': 0, 'tmax_sys': 0,
                                      'tmax_worker': 0, 'tmax_bulkjob_id': 0,
                                      'tmin_worker': 0, 'tmin_bulkjob_id': 0}
            time = item['real']
            if time > summaries[command]['tmax']:
                summaries[command]['tmax'] = time
                summaries[command]['tmax_user'] = item['user']
                summaries[command]['tmax_sys'] = item['sys']
                summaries[command]['tmax_worker'] = job['worker']
                summaries[command]['tmax_bulkjob_id'] = job['bulkjob_id']
            if time < summaries[command]['tmin']:
                summaries[command]['tmin'] = time
                summaries[command]['tmin_worker'] = job['worker']
                summaries[command]['tmin_bulkjob_id'] = job['bulkjob_id']
            summaries[command]['tave'] += time
            summaries[command]['tsd'] += time*time
    for k, v in summaries.items():
        v['tave'] /= num_job
        v['tsd'] = math.sqrt(v['tsd']/num_job - v['tave']*v['tave'])

    return summaries


def print_command_summary(summary):
    #print('min: %.3f' % summary['tmin'])
    print('min: %.3f @(worker:%d, bulkjob_id:%d)'
          % (summary['tmin'], summary['tmin_worker'], summary['tmin_bulkjob_id']))
    print('max: %.3f @(worker:%d, bulkjob_id:%d)'
          % (summary['tmax'], summary['tmax_worker'], summary['tmax_bulkjob_id']))
    print('ave: %.3f' % summary['tave'])
    print('sd : %.3f' % summary['tsd'])
    print('(real, user, sys) @(worker:%d, bulkjob_id:%d) = (%.3f, %.3f, %.3f)'
          % (summary['tmax_worker'], summary['tmax_bulkjob_id'],
             summary['tmax'], summary['tmax_user'], summary['tmax_sys']))


def print_job_title(job_name):
    print('')
    print('-' * 80)
    print('==== %s ====' % job_name)
    print('')
