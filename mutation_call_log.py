#!/usr/bin/env python

from common import *

commands = [
    'fisher comparison',
    'mutfilter realignment',
    'mutfilter indel',
    'mutfilter breakpoint',
    'mutfilter simplerepeat',
    'EBFilter',
]


def summarize(job_index, job_list, vge_output_dir):
    print_job_title('6A: mutation_call')

    index = job_index[0]
    num_job = len(index)
    print('# bulkjobs: %d' % num_job)

    print('')
    print('--- total time')
    summary = summarize_total_time(index, job_list)
    print_total_time_summary(summary)

    print('')
    print('--- @(worker:%d, bulkjob_id:%d)'
          % (summary['tmax_worker'], summary['tmax_bulkjob_id']))
    tmax_id = summary['tmax_id']
    job = job_list[tmax_id]
    file_name = vge_output_dir + job['file'] + '.e' + str(tmax_id)
    time_list = get_time(file_name)
    print('%-24s%12s%12s%12s' % ('commnad','real', 'user', 'sys'))
    print('-' * 60)
    for time in time_list:
        print('%-24s%12.3f%12.3f%12.3f'
              % (time['command'], time['real'], time['user'], time['sys']))

    summaries = summarize_command(index, job_list, vge_output_dir)

    for command in commands:
        print('')
        print('--- %s' % command)
        print_command_summary(summaries[command])

