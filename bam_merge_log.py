#!/usr/bin/env python

import os
import sys

from common import *

commands2 = [
    'samtools cat',
    'bai_merge',
    'md5sum',
]


def check_type(file_name):
    if (os.system('grep -q Miya1T ' + file_name) == 0):
        return 'T'
    elif (os.system('grep -q Miya1N ' + file_name) == 0):
        return 'N'
    else:
        print('job type unknown: %s' % file_name)
        sys.exit(1)


def summarize_stage2(index, job_list, vge_output_dir):
    assert len(index) == 1
    job = job_list[index[0]]

    print('')
    print('--- total time')
    print('%.3f @(worker:%d)' % (job['time'], job['worker']))

    print('')
    file_name = vge_output_dir + job['file'] + '.e' + str(job['id'])
    time_list = get_time(file_name)
    print('%-24s%12s%12s%12s' % ('commnad','real', 'user', 'sys'))
    print('-' * 60)
    for time in time_list:
        print('%-24s%12.3f%12.3f%12.3f'
              % (time['command'], time['real'], time['user'], time['sys']))


def summarize(job_index, job_list, vge_output_dir):
    print_job_title('8A: bam_merge')

    assert len(job_index) == 2
    type = {}
    for i in [0, 1]:
        file_name = vge_output_dir + job_list[job_index[i][0]]['file']
        type[check_type(file_name)] = i

    for t in ['N', 'T']:
        print('type: %s' % t)
        summarize_stage2(job_index[type[t]], job_list, vge_output_dir)
        print('')
