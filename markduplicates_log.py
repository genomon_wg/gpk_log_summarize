#!/usr/bin/env python

import os
import sys

from common import *

commands1 = [
    'cat',
    'bamsort',
    'bammarkduplicates',
    'remove_chr_dir',
]


def check_type(file_name):
    if (os.system('grep -q Miya1T ' + file_name) == 0):
        return 'T'
    elif (os.system('grep -q Miya1N ' + file_name) == 0):
        return 'N'
    else:
        print('job type unknown: %s' % file_name)
        sys.exit(1)


def summarize_stage1(index, job_list, vge_output_dir):
    num_job = len(index)
    print('# bulkjobs: %d' % num_job)

    print('')
    print('--- total time')
    summary = summarize_total_time(index, job_list)
    print_total_time_summary(summary)

    summaries = summarize_command(index, job_list, vge_output_dir)

    for command in commands1:
        print('')
        print('--- %s' % command)
        print_command_summary(summaries[command])


def summarize(job_index, job_list, vge_output_dir):
    print_job_title('5A: markduplicates')

    assert len(job_index) == 2
    type = {}
    for i in [0, 1]:
        file_name = vge_output_dir + job_list[job_index[i][0]]['file']
        type[check_type(file_name)] = i

    for t in ['N', 'T']:
        print('type: %s' % t)
        summarize_stage1(job_index[type[t]], job_list, vge_output_dir)
        print('')
