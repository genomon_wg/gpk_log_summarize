#!/usr/bin/env python

import os
import re
import sys
import datetime

import bam2fastq_log
import fastq_splitter_log
import bwa_align_log
import markduplicates_log
import mutation_call_log
import mutation_merge_log
import bam_merge_log


re_job_name = re.compile('(.*)_\d{8}_\d{4}_\d{6}\.sh\.\d+')

def get_datetime(s):
    try:
        dt = datetime.datetime.strptime(s, "%Y-%m-%d %H:%M:%S.%f")
    except ValueError:
        dt = datetime.datetime.strptime(s, "%Y-%m-%d %H:%M:%S")
    return dt


def get_job_name(s):
    return re_job_name.match(s).group(1)


if __name__ == '__main__':

    if len(sys.argv) != 2:
        print('usage: %s vge_output_directory' % sys.argv[0])
        sys.exit(1)

    print('vge_output: %s' % os.path.abspath(sys.argv[1]))
    vge_output_dir = sys.argv[1] + '/'
    input_csv = vge_output_dir + 'vge_joblist.csv'

    f = open(input_csv, 'r')
    f.readline()  # skip header

    job_index = {}
    job_list = []

    bulkjob_list = []

    num_job = 0
    num_unique_job = 0

    start = datetime.datetime.max
    finish = datetime.datetime.min

    for line in f:
        items = line.rstrip('\n').split(',')
        if len(items) == 15:    # VGE1
            (jobid, status, sendvgetime, bulkjob_id,
            finish_time, start_time, worker, return_code,
            filename, elapsed_time, genomon_pid, max_task,
            command_id, unique_jobid, sendtoworker) = items
        elif len(items) == 17:    # VGE2
            (jobid, status, sendvgetime, bulkjob_id, execjobid,
            finish_time, start_time, worker, return_code,
            filename, pipeline_parent_pid, elapsed_time, max_task, pipeline_pid,
            command_id, unique_jobid, sendtoworker) = items
        elif len(items) == 18:    # VGE2_mod
            (jobid, status, sendvgetime, command_id, bulkjob_id,
            execjobid, finish_time, start_time, worker, return_code,
            filename, pipeline_parent_pid, elapsed_time, max_task,
            pipeline_pid,sendtoworker,unique_jobid,priority) = items
        else:
            raise RuntimeError("invalid line: %s" % line)

        assert return_code == '0'

        jobid = int(jobid)
        bulkjob_id = int(bulkjob_id)
        unique_jobid = int(unique_jobid)
        worker = int(worker)

        assert jobid == num_job
        num_job += 1

        s = get_datetime(start_time)
        f = get_datetime(finish_time)

        if s < start: start = s
        if f > finish: finish = f

        #time = (f - s).total_seconds()
        time = float(elapsed_time)

        job = dict(id = jobid, file = filename, time = time, worker =  worker,
                   bulkjob_id = bulkjob_id)
        job_list.append(job)

        if bulkjob_id == 0:
            assert unique_jobid == num_unique_job
            num_unique_job += 1

            bulkjob_list.append([])

            name = get_job_name(filename)
           #job_index[name].append(unique_jobid)
            job_index.setdefault(name, []).append(unique_jobid)

        bulkjob_list[unique_jobid].append(jobid)

    # unique_jobid list -> jobid list
    for name, unique_joblist in job_index.items():
        tmp = []
        for unique_job in unique_joblist:
            tmp.append(bulkjob_list[unique_job])
        job_index[name] = tmp

    print('')
    print('first job started at: %s' % start.strftime('%Y/%m/%d %H:%M:%S'))
    print('last job finished at: %s' % finish.strftime('%Y/%m/%d %H:%M:%S'))

    print('')
    total_sec = int((finish - start).total_seconds())
    min = total_sec // 60
    sec = total_sec % 60
    hour = min // 60
    min = min % 60
    print('total: %d[sec] (%2d:%02d:%02d)' % (total_sec, hour, min, sec))

    if 'bam2fastq' in job_index:
        bam2fastq_log.summarize(job_index['bam2fastq'], job_list, vge_output_dir)
    if 'fastq_splitter' in job_index:
        fastq_splitter_log.summarize(job_index['fastq_splitter'], job_list, vge_output_dir)
    if 'bwa_align' in job_index:
        bwa_align_log.summarize(job_index['bwa_align'], job_list, vge_output_dir)
    if 'markduplicates' in job_index:
        markduplicates_log.summarize(job_index['markduplicates'], job_list, vge_output_dir)
    if 'mutation_call' in job_index:
        mutation_call_log.summarize(job_index['mutation_call'], job_list, vge_output_dir)
    if 'mutation_merge' in job_index:
        mutation_merge_log.summarize(job_index['mutation_merge'], job_list, vge_output_dir)
    if 'bam_merge' in job_index:
        bam_merge_log.summarize(job_index['bam_merge'], job_list, vge_output_dir)

