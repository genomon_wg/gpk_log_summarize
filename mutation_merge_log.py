#!/usr/bin/env python

from common import *


def summarize(job_index, job_list, vge_output_dir):
    print_job_title('7A: mutation_merge')

    assert len(job_index) == 1

    job = job_list[job_index[0][0]]

    print('--- total time')
    print('%.3f @(worker:%d)' % (job['time'], job['worker']))

    print('')
    file_name = vge_output_dir + job['file'] + '.e' + str(job['id'])
    time_list = get_accumulate_time(file_name)
    print('%-24s%12s%12s%12s' % ('commnad','real', 'user', 'sys'))
    print('-' * 60)
    for command, time in time_list.items():
        print('%-24s%12.3f%12.3f%12.3f'
              % (command, time['real'], time['user'], time['sys']))

