# gpk_log_summarize

GenomonPipelineのVGE出力ファイルを集計して、その結果を標準出力に出力します。

## Usage
    gpk_log_summarize.py <vge_output_directory>
